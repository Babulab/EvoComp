# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 10:34:25 2018

@author: Marc
"""

import random
import operator
import math

import numpy

from deap import base
from deap import creator
from deap import gp
from deap import tools
from fenmod import *



# Define new functions
def protectedDiv(left, right):
    try:
        return left / right
    except ZeroDivisionError:
        return 1



pset1 = gp.PrimitiveSet("Cdrag", 3) #Coef Drag
pset1.addPrimitive(operator.add, 2)
pset1.addPrimitive(operator.sub, 2)
pset1.addPrimitive(operator.mul, 2)
pset1.addPrimitive(protectedDiv, 2)
pset1.addPrimitive(operator.neg, 1)
pset1.addPrimitive(math.cos, 1)
pset1.addPrimitive(math.sin, 1)
pset1.addEphemeralConstant("randtr1", lambda: numpy.random.rand())
pset1.renameArguments(ARG0='Re') #Nro Reynolds
pset1.renameArguments(ARG1='A_norm') #Área normalizada
pset1.renameArguments(ARG2='df') #Densidad de fluido

pset2 = gp.PrimitiveSet("Friction", 4) #Coef friccion
pset2.addPrimitive(operator.add, 2)
pset2.addPrimitive(operator.sub, 2)
pset2.addPrimitive(operator.mul, 2)
pset2.addPrimitive(protectedDiv, 2)
pset2.addPrimitive(operator.neg, 1)
pset2.addPrimitive(math.cos, 1)
pset2.addPrimitive(math.sin, 1)
pset2.addEphemeralConstant("randtr2", lambda: numpy.random.rand())
pset2.renameArguments(ARG0='Re') #Nro Reynolds
pset2.renameArguments(ARG1='S') #Separación entre celdas
pset2.renameArguments(ARG2='V') #Velocidad del fluido
pset2.renameArguments(ARG3='df') #Densidad de fluido

pset3 = gp.PrimitiveSet("Nu", 1) #Coef Nu
pset3.addPrimitive(operator.add, 2)
pset3.addPrimitive(operator.sub, 2)
pset3.addPrimitive(operator.mul, 2)
pset3.addPrimitive(protectedDiv, 2)
pset3.addPrimitive(operator.neg, 1)
pset3.addPrimitive(math.cos, 1)
pset3.addPrimitive(math.sin, 1)
pset3.addEphemeralConstant("randtr3", lambda: numpy.random.rand())
pset3.renameArguments(ARG0='Re') #Nro Reynolds
psets = (pset1, pset2,pset3)

creator.create("FitnessMin", base.Fitness, weights=(-1.0,-1.0,-1.0))

creator.create("Tree", gp.PrimitiveTree)

creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()
toolbox.register('tree1_expr', gp.genHalfAndHalf, pset=pset1, min_=1, max_=3)
toolbox.register('tree2_expr', gp.genHalfAndHalf, pset=pset2, min_=1, max_=3)
toolbox.register('tree3_expr', gp.genHalfAndHalf, pset=pset3, min_=1, max_=3)

toolbox.register('Cdrag', tools.initIterate, creator.Tree, toolbox.tree1_expr)
toolbox.register('Friction', tools.initIterate, creator.Tree, toolbox.tree2_expr)
toolbox.register('Nu', tools.initIterate, creator.Tree, toolbox.tree3_expr)

func_cycle = [toolbox.Cdrag, toolbox.Friction,toolbox.Nu]

toolbox.register('individual', tools.initCycle, creator.Individual, func_cycle)
toolbox.register('population', tools.initRepeat, list, toolbox.individual)




def evalSymbReg(individual,mod,out):
    # Transform the tree expression in a callable function
    func1 = toolbox.compile(individual[0], pset1)
    func2 = toolbox.compile(individual[1], pset2)
    func3 = toolbox.compile(individual[2],pset3)
    """
    res1 = func1(100,50,20)
    res2 = func2(65,23,55,78)
    res3=func3(5)
    error1=1
    error2=2
    error3=3
    """
    mod.uploadInd(func1,func2,func3)
    mod.reset()
    res = mod.evolve(10)
    mse = MSE(res, out)
    print(mse)
    return [mse[0].item(), mse[1].item(),mse[2].item()]



toolbox.register('compile', gp.compile)
toolbox.register('evaluate', evalSymbReg)
toolbox.register('select', tools.selTournament, tournsize=3)  
toolbox.register('mate', gp.cxOnePoint) #Mate es un nombre arbitrario que se da para el cruzamiento, el cruzamiento puede ser cxOnePoint,cxMessyOnePoint,cxTwoPoint entre otros 
toolbox.register('expr', gp.genFull, min_=1, max_=2)
toolbox.register('mutate', gp.mutUniform, expr=toolbox.expr) #mutGaussian, mutUniformInt,mutESLogNormal entre otros




dataInPath = "/root/Documentos/C_digo_Java_Proyecto/Batteries_GP_MO_Java/TrainingData/doe500trainnew.txt"
dataOutPath = "/root/Documentos/C_digo_Java_Proyecto//Batteries_GP_MO_Java/TrainingData/salidas_ansys500trainnew.txt"


def main():
    random.seed(1024)
    ind = toolbox.individual()

    pop = toolbox.population(n=10)#
    hof = tools.HallOfFame(1) # En hof quedara solo un mejor individuo
    
    
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean,axis=0)
    stats.register("std", numpy.std,axis=0)
    stats.register("min", numpy.min,axis=0)
    stats.register("max", numpy.max,axis=0)

    logbook = tools.Logbook()
    logbook.header = "gen", "evals", "std", "min", "avg", "max"

    CXPB, MUTPB, NGEN = 1, 0.2, 10

    # Evaluate the entire population
    
    data_in  = pd.read_csv(dataInPath, sep=',', header=None, names=['I', 'Separation', 'Fin', 'Tin', 'Diameter'])
    data_out = pd.read_csv(dataOutPath, sep=',', header=None, names=['TFin', 'Tc', 'Pf', 'Vf', 'Dens', 'CoefArr', 'Reynolds', 'Nusselt'])
    out = torch.Tensor(data_out[['Vf','Pf','Tc']].values)
    mdl = Model(data_in, nind=len(pop))
    
    for ind in pop:
        """
        f1 = toolbox.evaluate(ind[0])
        f2 = toolbox.evaluate(ind[1])
        f3 = toolbox.evaluate(ind[2])        
        
        """
        ind.fitness.values = toolbox.evaluate(ind,mdl,out)
        
        

        
        #ind.fitness.values = toolbox.evaluate(ind)

    hof.update(pop)
    record = stats.compile(pop)
    logbook.record(gen=0, evals=len(pop), **record)
    print(logbook.stream)

    for g in range(1, NGEN):
        # Select the offspring
        offspring = toolbox.select(pop, len(pop)) #ordena los padres
        # Clone the offspring
        offspring = [toolbox.clone(ind) for ind in offspring]

        # Apply crossover and mutation
        for ind1, ind2 in zip(offspring[::2], offspring[1::2]): #Hay que chequear el salto, creo que esta repitiendo ciertos indices
          #  for n_tree in range(len(ind1)):  
          
                print('individuo 1, arbol1:',ind1[1].__str__())
                print('individuo 2, arbol1:',ind2[1].__str__())
                if random.random() < CXPB:
                    
                    toolbox.mate(ind1[0], ind2[0]) #mas arriba se define la forma de cruzamiento, en toolbox.register
                    toolbox.mate(ind1[1], ind2[1])
                    toolbox.mate(ind1[2], ind2[2])
                    del ind1.fitness.values
                    del ind2.fitness.values
                        
                        
        for ind in offspring:
            for tree, pset in zip(ind, psets):
                if random.random() < MUTPB:
                    toolbox.mutate(individual=tree, pset=pset)
                    del ind.fitness.values

        # Evaluate the individuals with an invalid fitness
        invalids = [ind for ind in offspring if not ind.fitness.valid]
        for ind in invalids:
            ind.fitness.values = toolbox.evaluate(ind)

        # Replacement of the population by the offspring

        pop=pop+offspring
         #Concatenar Padres e hijos, luego selNSGA2
        pop=tools.selNSGA2(pop,len(pop))
        pop=pop[:int(len(pop)/2)]
        hof.update(pop)
        record = stats.compile(pop)
        logbook.record(gen=g, evals=len(invalids), **record)
        print(logbook.stream)

    print('Best individual_tree1 : ', hof[0][0])
    print('Best individual_tree2 : ', hof[0][1])
    print('Best individual_tree2 : ', hof[0][2])
    print('Fitness', hof[0].fitness)

    return pop, stats, hof


if __name__ == "__main__":
    main()
    
    
    
    
    
