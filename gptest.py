

import random
import operator
import math

import numpy

from deap import base
from deap import creator
from deap import gp
from deap import tools


# Define new functions
def protectedDiv(left, right):
    try:
        return left / right
    except ZeroDivisionError:
        return 1



pset1 = gp.PrimitiveSet("Cdrag", 3) #Coef Drag
pset1.addPrimitive(operator.add, 2)
pset1.addPrimitive(operator.sub, 2)
pset1.addPrimitive(operator.mul, 2)
pset1.addPrimitive(protectedDiv, 2)
pset1.addPrimitive(operator.neg, 1)
pset1.addPrimitive(math.cos, 1)
pset1.addPrimitive(math.sin, 1)
pset1.addEphemeralConstant("randtr1", lambda: numpy.random.rand())
pset1.renameArguments(ARG0='Re') #Nro Reynolds
pset1.renameArguments(ARG1='A_norm') #Área normalizada
pset1.renameArguments(ARG2='df') #Densidad de fluido

pset2 = gp.PrimitiveSet("Friction", 4) #Coef friccion
pset2.addPrimitive(operator.add, 2)
pset2.addPrimitive(operator.sub, 2)
pset2.addPrimitive(operator.mul, 2)
pset2.addPrimitive(protectedDiv, 2)
pset2.addPrimitive(operator.neg, 1)
pset2.addPrimitive(math.cos, 1)
pset2.addPrimitive(math.sin, 1)
pset2.addEphemeralConstant("randtr2", lambda: numpy.random.rand())
pset2.renameArguments(ARG0='Re') #Nro Reynolds
pset2.renameArguments(ARG1='S') #Separación entre celdas
pset2.renameArguments(ARG2='V') #Velocidad del fluido
pset2.renameArguments(ARG3='df') #Densidad de fluido

psets = (pset1, pset2)

creator.create("FitnessMin", base.Fitness, weights=(-1.0,-1.0,))
creator.create("Tree", gp.PrimitiveTree)

creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()
toolbox.register('tree1_expr', gp.genHalfAndHalf, pset=pset1, min_=1, max_=3)
toolbox.register('tree2_expr', gp.genHalfAndHalf, pset=pset2, min_=1, max_=3)

toolbox.register('Cdrag', tools.initIterate, creator.Tree, toolbox.tree1_expr)
toolbox.register('Friction', tools.initIterate, creator.Tree, toolbox.tree2_expr)

func_cycle = [toolbox.Cdrag, toolbox.Friction]

toolbox.register('individual', tools.initCycle, creator.Individual, func_cycle)
toolbox.register('population', tools.initRepeat, list, toolbox.individual)


def evalSymbReg(individual):
    # Transform the tree expression in a callable function
    func1 = toolbox.compile(individual[0], pset1)
    func2 = toolbox.compile(individual[1], pset2)
    res1 = func1(100,50,20)
    res2 = func2(65,23,55,78)
    return [res1+res2,res1-res2]


toolbox.register('compile', gp.compile)
toolbox.register('evaluate', evalSymbReg)
toolbox.register('select', tools.selTournament, tournsize=3)  
toolbox.register('mate', gp.cxOnePoint) #Mate es un nombre arbitrario que se da para el cruzamiento, el cruzamiento puede ser cxOnePoint,cxMessyOnePoint,cxTwoPoint entre otros 
toolbox.register('expr', gp.genFull, min_=1, max_=2)
toolbox.register('mutate', gp.mutUniform, expr=toolbox.expr) #mutGaussian, mutUniformInt,mutESLogNormal entre otros





def main():
    random.seed(1024)
    ind = toolbox.individual()

    pop = toolbox.population(n=10)#
    hof = tools.HallOfFame(1) # En hof quedara solo un mejor individuo
    
    
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean,axis=0)
    stats.register("std", numpy.std,axis=0)
    stats.register("min", numpy.min,axis=0)
    stats.register("max", numpy.max,axis=0)

    logbook = tools.Logbook()
    logbook.header = "gen", "evals", "std", "min", "avg", "max"

    CXPB, MUTPB, NGEN = 0.5, 0.2, 10

    # Evaluate the entire population
    for ind in pop:
        ind.fitness.values = toolbox.evaluate(ind)

    hof.update(pop)
    record = stats.compile(pop)
    logbook.record(gen=0, evals=len(pop), **record)
    print(logbook.stream)

    for g in range(1, NGEN):
        # Select the offspring
        offspring = toolbox.select(pop, len(pop))
        # Clone the offspring
        offspring = [toolbox.clone(ind) for ind in offspring]

        # Apply crossover and mutation
        for ind1, ind2 in zip(offspring[::2], offspring[1::2]):
            for n_tree in range(len(ind1)):
                for tree1, tree2 in zip(ind1, ind2): #Revisar si es q zip referencia a offspring
                    
                    if random.random() < CXPB:
                        toolbox.mate(tree1, tree2) #mas arriba se define la forma de cruzamiento, en toolbox.register
                        del ind1.fitness.values
                        del ind2.fitness.values

        for ind in offspring:
            for tree, pset in zip(ind, psets):
                if random.random() < MUTPB:
                    toolbox.mutate(individual=tree, pset=pset)
                    del ind.fitness.values

        # Evaluate the individuals with an invalid fitness
        invalids = [ind for ind in offspring if not ind.fitness.valid]
        for ind in invalids:
            ind.fitness.values = toolbox.evaluate(ind)

        # Replacement of the population by the offspring

        pop=pop+offspring
         #Concatenar Padres e hijos, luego selNSGA2
        pop=tools.selNSGA2(pop,len(pop))
        pop=pop[:int(len(pop)/2)]
        hof.update(pop)
        record = stats.compile(pop)
        logbook.record(gen=g, evals=len(invalids), **record)
        print(logbook.stream)

    print('Best individual_tree1 : ', hof[0][0])
    print('Best individual_tree2 : ', hof[0][1])
    print('Fitness', hof[0].fitness)

    return pop, stats, hof


if __name__ == "__main__":
    main()
if __name__ == "__main__":
    main()
