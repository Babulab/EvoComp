import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


class Model:
    def __init__(self, I=5, D=18, S=1, Fin=100, T0=20, colfluido=2, nfluido=2, nind=1):
        """
		"""
        # inputs
        self.D = 1.0 * D / 1000
        self.I = 1.0 * I
        self.S = 1.0 * S
        self.Fin = Fin * 0.00047
        self.T0 = 1.0 * T0

        # fixed parameters
        self.R = 32 * 1e-3  # Resistencia interna
        self.Lcell = 65 * 1e-3  # largo celda
        self.e = 15 * 1e-3  # espacio pared-celda
        self.z = 5 * 1e-3  # Corte de estudio
        self.Patm = 0  # presion atmosferica
        self.colfluido = colfluido
        self.colcelda = self.colfluido - 1
        self.nfluido = nfluido
        self.ncelda = self.nfluido - 1
        self.errmax = 1e-3
        self.nind = nind

    def defineFM(self):
        # modelo fenomenologico
        self.Tf = np.zeros((self.nind, self.colfluido))
        self.df = np.zeros((self.nind, self.colfluido))
        self.Fdrag = np.zeros((self.nind, self.colfluido))
        self.Vf = np.zeros((self.nind, self.colfluido))
        self.Vmf = np.zeros((self.nind, self.colfluido))
        self.Pf = np.zeros((self.nind, self.colfluido))
        self.Tc = np.zeros((self.nind, self.colfluido))
        self.Re = np.zeros((self.nind, self.colfluido))

        self.a1, self.a2 = q_paramdrag(self.S)
        self.a3 = 0.653
        self.a = np.array([self.a1, self.a2, self.a3])
        self.q_p = self.I ** 2 * self.R * self.z / self.Lcell
        self.Asup = np.pi * self.D * self.z
        self.Avolcont = (self.S + 1) * self.D * self.z
        self.Anorm = self.Asup / self.Avolcont
        self.height = 2 * self.e + self.D * (self.nfluido + self.ncelda * self.S)
        self.Vinit = (self.a2 * self.Fin / self.Lcell / self.height).item()
        self.Tf[:, 0] = self.T0  # evolve
        self.df[:, 0] = q_densidad(self.T0)  # evolve
        self.Re[:, 0] = np.zeros(self.nind)  # evolve
        self.m_p = (self.S + 1) * self.D * self.z * self.Vinit * self.df[:, 0]

    def evolve(self, nmax):
        for _ in range(nmax):
            self.dfnorm = self.df[:, 0] / 1.205
            Cdrag = self.evalCdrag(self.Re[:, 0], self.Anorm, self.dfnorm)
            self.Fdrag[:, 0] = 0.5 * Cdrag * self.Vinit ** 2 * self.D * self.z * self.df[:, 0]
            self.Vf[:, 0] = self.Vinit - self.Fdrag[:, 0] / self.m_p
            self.Vmf[:, 0] = self.Vf[:, 0] * self.S / (self.S + 1)
            self.Re[:, 0] = q_reynolds(self.Vmf[:, 0], self.Tf[:, 0], self.D, self.df[:, 0])
            Friction = self.evalFriction(self.Re[:, 0], self.S, self.Vmf[:, 0] / self.Vinit, self.dfnorm).squeeze()
            self.Pf[:, 0] = self.Pf[:, 1] + 0.5 * Friction * self.df[:, 0] * self.Vmf[:, 0] ** 2
            for i in range(self.colfluido - 1):
                self.Re[:, i + 1] = q_reynolds(self.Vmf[:, i], self.Tf[:, i], self.D, self.df[:, i])
                self.dfnorm = self.df[:, i] / 1.205
                Friction = self.evalFriction(self.Re[:, i], self.S, self.Vmf[:, i] / self.Vinit, self.dfnorm).squeeze()
                self.Pf[:, i] = self.Pf[:, i + 1] + 0.5 * Friction * self.df[:, i] * self.Vmf[:, i] ** 2
                Cdrag = self.evalCdrag(self.Re[:, i], self.Anorm, self.dfnorm)
                self.Fdrag[:, i + 1] = 0.5 * Cdrag * self.Vf[:, i] ** 2 * self.D * self.z * self.df[:, i]
                self.Vf[:, i + 1] = self.Vf[:, i] + (self.Avolcont * (self.Pf[:, i] - self.Pf[:, i + 1]) - self.Fdrag[:, i + 1]) / self.m_p
                cp = q_cp(self.Tf[:, i])
                self.Tf[:, i + 1] = self.Tf[:, i] + (self.q_p / self.m_p - 0.5 * (self.Vf[:, i + 1] ** 2 - self.Vf[:, i] ** 2)) / cp
                self.df[:, i + 1] = q_densidad(self.Tf[:, i + 1])
                Nu = self.evalNu(self.Re[:, i])
                kf = q_conductividad(self.Tf[:, i])
                hf = Nu * kf / self.D
                self.Tc[:, i] = self.q_p / self.Asup / hf + 0.5 * (self.Tf[:, i] + self.Tf[:, i + 1])
        # return self.Fdrag, self.Vf, self.Vmf, self.Re, self.Pf, self.Tc, self.Tf
        return np.c_[self.Vf[:, 1], self.Pf[:, 0], self.Tc[:, 0]]

    def reset(self):
        self.defineFM()

    def uploadPob(self, pobcdrag, pobfriction, pobnu):
        """
		upload list of functions 
		"""
        if hasattr(pobcdrag, '__len__'):
            self.pobcdrag = pobcdrag
            self.pobfriction = pobfriction
            self.pobnu = pobnu
        else:
            self.pobcdrag = [pobcdrag]
            self.pobfriction = [pobfriction]
            self.pobnu = [pobnu]
        self.nind = len(self.pobcdrag)

    def evalFriction(self, _re, _s, _vmf, _dfnorm):
        _output = []
        for _i, _ind in enumerate(self.pobfriction):
            _output.append(_ind(_re[_i], _s, _vmf[_i], _dfnorm[_i]))
        return np.stack(_output)

    def evalCdrag(self, _re, _anorm, _df):
        _output = []
        for _i, _ind in enumerate(self.pobcdrag):
            _output.append(_ind(_re[_i], _anorm, _df[_i]))
        return np.stack(_output)

    def evalNu(self, _re):
        _output = []
        for _i, _ind in enumerate(self.pobnu):
            _output.append(_ind(_re[_i]))
        return np.stack(_output)


def interp1d(_x, _y, _x0):
    """linear regression"""
    _mean_x = np.mean(_x)
    _mean_y = np.mean(_y)
    _m = np.sum((_x - _mean_x) * (_y - _mean_y)) / np.sum((_x - _mean_x) ** 2)
    _b = _mean_y - _m * _mean_x
    return np.asarray(_m * _x0 + _b)


def q_conductividad(_t):
    _t += 273.15
    _datos = np.array([22.3, 26.3, 30, 33.8, 37.3]) * 1e-3
    _temp = np.array([250, 300, 350, 400, 450]) * 1.
    _k = interp1d(_temp, _datos, _t)
    np.place(_k, _k < 0, 0)
    np.place(_k, np.isnan(_k), 0.0001)
    return _k


def q_reynolds(_v, _t, _D, _d):
    _visc = q_viscosidad(_t)
    _re = _d * _v * _D / _visc
    return _re


def q_viscosidad(_t):
    _t += 273.15
    _datos = np.array([159.6, 184.6, 208.2, 230.1, 250.7]) * 1e-7
    _temp = np.array([250, 300, 350, 400, 450])
    _visc = interp1d(_temp, _datos, _t)
    np.place(_visc, np.isnan(_visc), 0)
    return _visc


def q_paramdrag(_s):
    _b1 = np.array([0.039, 0.028, 0.027, 0.028, 0.005])
    _b2 = np.array([3.270, 2.416, 2.907, 2.974, 2.063])
    _x = np.array([0.10, 0.25, 0.50, 0.75, 1.00])
    _a1 = interp1d(_x, _b1, _s)
    _a2 = interp1d(_x, _b2, _s)
    if _s > _x[-1]:
        _a1 = _b1[-1]
        _a2 = _b2[-1]
    if _a1 < 0:
        _a1 = 0
    if _a2 < 0:
        _a2 = 0
    return _a1, _a2


def q_densidad(_t):
    _datos = np.array([1.293, 1.205, 1.127])
    _temp = np.array([0, 20, 40]) * 1.
    _d = interp1d(_temp, _datos, _t)
    np.place(_d, np.isnan(_d), 1)
    return _d


def q_cp(_t):
    _t += 273.15
    _datos = np.array([1.006, 1.007, 1.009, 1.014, 1.021]) * 1e3
    _temp = np.array([250, 300, 350, 400, 450]) * 1.
    _cp = interp1d(_temp, _datos, _t)
    np.place(_cp, np.isnan(_cp), 1)
    return _cp


def f1drag(_x, _y, _z):
    return 0.891 * _y - _y ** 2 + _x ** 0.1452


def f2drag(_x, _y, _z):
    return _y - _y ** 2 + _x ** 0.1452


def f3drag(_x, _y, _z):
    return 2 * _y - _y ** 2 + _x ** 0.1452


def f1friction(_x, _y, _z, _w):
    return _w * _y ** (24.5261 * _x ** -0.3891 - 2)


def f2friction(_x, _y, _z, _w):
    return _w * _y ** (24 * _x ** -0.3891 - 2)


def f3friction(_x, _y, _z, _w):
    return _w * _y ** (_x ** -0.3891 - 2)


def f1nu(_x):
    return 2.0232 * _x ** 0.5528


def f2nu(_x):
    return _x ** 0.5528


def f3nu(_x):
    return 4 * _x ** 0.5528


if __name__ == '__main__':
    POP = [[f1drag, f1friction, f1nu], \
           [f2drag, f2friction, f2nu]]

    data_in  = pd.read_csv('doe500train.txt', sep=',', header=None, names=['I', 'Separation', 'Fin', 'Tin', 'Diameter'])
    data_out = pd.read_csv('salidas_ansys500train.txt', sep=',', header=None, names=['TFin', 'Tc', 'Pf', 'Vf', 'Dens', 'CoefArr', 'Reynolds', 'Nusselt'])

    mdl = Model()
    error_pop = []
    for individual in POP:
        res = []
        for param_in in data_in.values:
            mdl.reset()
            mdl.I, mdl.S, mdl.Fin, mdl.T0, mdl.S = param_in
            mdl.uploadPob(*individual)
            mdl.defineFM()
            res.append(mdl.evolve(10))
        error_pop.append(np.power(np.vstack(res)-data_out[['Vf','Pf','Tc']].values,2).mean(axis=0))


