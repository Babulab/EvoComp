
import numpy as np
import matplotlib.pyplot as plt
import torch
import time
import pandas as pd

class Model:
	def __init__(self, data_in, colfluido=2, nfluido=2, nind=1):
		"""
		"""
		self.I = torch.Tensor(data_in["I"].values)
		self.S = torch.Tensor(data_in["Separation"].values)
		self.Fin = torch.Tensor(data_in["Fin"].values)
		self.T0 = torch.Tensor(data_in["Tin"].values)
		self.D = torch.Tensor(data_in["Diameter"].values)

		self.Ndatos = len(self.I)

		# inputs
		self.D = self.D/1000
		self.Fin = self.Fin*0.00047

		# fixed parameters
		self.R = 32*1e-3 # Resistencia interna
		self.Lcell = 65*1e-3 #largo celda
		self.e = 15*1e-3 #espacio pared-celda
		self.z = 5*1e-3 #Corte de estudio
		self.Patm = 0 #presion atmosferica
		self.colfluido = colfluido
		self.colcelda = self.colfluido - 1
		self.nfluido = nfluido
		self.ncelda = self.nfluido - 1
		self.errmax = 1e-3
		self.nind = nind # number of individuals

		# pre calculated interpolations
		self.qreynolds = qReynolds()
		self.qconductividad = qConductividad()
		self.qparamdrag = qParamdrag()


	def defineFM(self):
		# modelo fenomenologico
		self.Tf = torch.zeros((self.colfluido, self.Ndatos)) + self.T0
		self.df = torch.zeros((self.colfluido, self.Ndatos)) + q_densidad(self.T0)
		self.Fdrag = torch.zeros((self.colfluido, self.Ndatos))
		self.Vf = torch.zeros((self.colfluido, self.Ndatos))
		self.Vmf = torch.zeros((self.colfluido, self.Ndatos))
		self.Pf = torch.zeros((self.colfluido, self.Ndatos))
		self.Tc = torch.zeros((self.colfluido, self.Ndatos)) + self.T0
		self.Re = torch.zeros((self.colfluido, self.Ndatos))

		self.a1, self.a2 = self.qparamdrag.value(self.S)
		self.a3 = torch.ones(self.Ndatos)*0.653
		self.a = torch.cat((self.a1, self.a2, self.a3)).reshape((3, self.Ndatos))
		self.q_p = self.I**2*self.R*self.z/self.Lcell
		self.Asup = np.pi*self.D*self.z
		self.Avolcont = (self.S + 1)*self.D*self.z
		self.Anorm = self.Asup/self.Avolcont
		self.height = 2*self.e + self.D*(self.nfluido + self.ncelda*self.S)
		self.Vinit = (self.a2*self.Fin/self.Lcell/self.height)
		self.m_p = (self.S + 1)*self.D*self.z*self.Vinit*self.df[0]

		self.Vmf += self.Vinit
		self.Vf += self.Vinit


	def evolve(self, nmax):
		for j in range(nmax):
			self.dfnorm = self.df[0]/1.205
			Cdrag = self.evalCdrag(self.Re[0], self.Anorm, self.dfnorm)
			self.Fdrag[0] = 0.5*Cdrag*self.Vinit**2*self.D*self.z*self.df[0]
			self.Vf[0] = self.Vinit - self.Fdrag[0]/self.m_p
			self.Vmf[0] = self.Vf[0]*self.S/(self.S + 1)
			self.Re[0] = self.qreynolds.value(self.Vmf[0], self.Tf[0], self.D, self.df[0])
			Friction = self.evalFriction(self.Re[0], self.S, self.Vmf[0]/self.Vinit, self.dfnorm).squeeze()
			self.Pf[0] = self.Pf[1] + 0.5*Friction*self.df[0]*self.Vmf[0]**2
			for i in range(self.colfluido - 1):
				self.Vmf[i + 1] = self.Vf[i]*self.S/(self.S + 1)
				#self.Re[:, i + 1] = q_reynolds(self.Vmf[:, i], self.Tf[:, i], self.D, self.df[:, i])
				self.Re[i + 1] = self.qreynolds.value(self.Vmf[i], self.Tf[i], self.D, self.df[i])
				self.dfnorm = self.df[i]/1.205
				Friction = self.evalFriction(self.Re[i], self.S, self.Vmf[i]/self.Vinit, self.dfnorm).squeeze()
				self.Pf[i] = self.Pf[i + 1] + 0.5*Friction*self.df[i]*self.Vmf[i]**2
				Cdrag = self.evalCdrag(self.Re[i], self.Anorm, self.dfnorm)
				self.Fdrag[i + 1] = 0.5*Cdrag*self.Vf[i]**2*self.D*self.z*self.df[i]
				self.Vf[i + 1] = self.Vf[i] + (self.Avolcont*(self.Pf[i] - self.Pf[i + 1]) - self.Fdrag[i + 1])/self.m_p
				cp = q_cp(self.Tf[i])
				self.Tf[i + 1] = self.Tf[i] + (self.q_p/self.m_p - 0.5*(self.Vf[i + 1]**2 - self.Vf[i]**2))/cp
				self.df[i + 1] = q_densidad(self.Tf[i + 1])
				Nu = self.evalNu(self.Re[i])
				kf = self.qconductividad.value(self.Tf[i])
				hf = Nu*kf/self.D
				self.Tc[i] = self.q_p/self.Asup/hf + 0.5*(self.Tf[i] + self.Tf[i + 1])
		"""
		Al final necesitamos guardar: velocidad a la salida (1), presion a la entrada (0), temperatura central (0).
		individual.set(vf.unsafe_get(1),pf.unsafe_get(0),tc.unsafe_get(0))
		"""
		return self.Vf[1], self.Pf[0], self.Tc[0]


	def reset(self):
		self.defineFM()


	def uploadInd(self, f1, f2, f3):
		"""
		Updates the functions for one individual.
		Inputs:
			f1: function, Friction tree.
			f2: function, Cdrag tree.
			f3: function, Nu tree.
		"""
		self.evalFriction = f2
		self.evalCdrag = f1
		self.evalNu = f3

	"""
	def uploadPob(self, pop):
		# upload list of functions 
		self.pobcdrag = []
		self.pobfriction = []
		self.pobnu = []
		for _ind in pop:
			self.pobcdrag.append(_ind[0])
			self.pobfriction.append(_ind[1])
			self.pobnu.append(_ind[2])


	def evalFriction(self, _re, _s, _vmf, _dfnorm):
		_output = []
		for _i, _ind in enumerate(self.pobfriction):
			_output.append(_ind(_re[_i], _s, _vmf[_i], _dfnorm[_i]))
		return torch.stack(_output).float()


	def evalCdrag(self, _re, _anorm, _df):
		_output = []
		for _i, _ind in enumerate(self.pobcdrag):
			_output.append(_ind(_re[_i], _anorm, _df[_i]))
		return torch.stack(_output).float()


	def evalNu(self, _re):
		_output = []
		for _i, _ind in enumerate(self.pobnu):
			_output.append(_ind(_re[_i]))
		return torch.stack(_output).float()
	"""


def interp1d(_x, _y, _x0):
	"""linear regression"""
	_mean_x = torch.mean(_x)
	_mean_y = torch.mean(_y)
	_x_subs = _x - _mean_x
	_m = torch.sum(_x_subs*(_y - _mean_y))/torch.sum(_x_subs**2)
	_b = _mean_y - _m*_mean_x
	return _m*_x0 + _b


def interp1D(_x, _y):
	"""
	linear regression
	inputs:
		_x: Tensor, independent variable
		_y: Tensor, dependent variable
	returns:
		_m: Tensor, slope
		_b: Tensor, intercept
	"""
	_mean_x = torch.mean(_x)
	_mean_y = torch.mean(_y)
	_x_subs = _x - _mean_x
	_m = torch.sum(_x_subs*(_y - _mean_y))/torch.sum(_x_subs**2)
	_b = _mean_y - _m*_mean_x
	return _m, _b

"""
def q_conductividad(_t):
	_t += 273.15
	_datos = torch.Tensor([22.3, 26.3, 30, 33.8, 37.3])*1e-3
	_temp = torch.Tensor([250, 300, 350, 400, 450])*1.
	_k = interp1d(_temp, _datos, _t)
	_k[_k<0] = 0.0001
	_k[torch.isnan(_k)] = 0.0001
	return _k
"""

class qConductividad():
	def __init__(self):
		_datos = torch.Tensor([22.3, 26.3, 30, 33.8, 37.3])*1e-3
		_temp = torch.Tensor([250, 300, 350, 400, 450])*1.
		self.m, self.b = interp1D(_temp, _datos)

	def value(self, x):
		x += 273.15
		_k = self.m*x + self.b
		_k[_k<0] = 0.0001
		_k[torch.isnan(_k)] = 0.0001
		return _k


class qReynolds():
	def __init__(self):
		self.qviscosidad = qViscosidad()

	def value(self, _v, _t, _D, _d):
		_visc = self.qviscosidad.value(_t)
		_re = _d*_v*_D/_visc
		return _re

"""
def q_reynolds(_v, _t, _D, _d):
	_visc = q_viscosidad(_t)
	_re = _d*_v*_D/_visc
	return _re
"""

class qViscosidad():
	def __init__(self):
		_datos = torch.Tensor([159.6, 184.6, 208.2, 230.1, 250.7])*1e-7
		_temp = torch.Tensor([250., 300., 350., 400., 450.])
		self.m, self.b = interp1D(_temp, _datos)

	def value(self, x):
		x += 273.15
		_visc = self.m*x + self.b
		_visc[torch.isnan(_visc)] = 0.	
		return _visc

"""
def q_viscosidad(_t):
	_t += 273.15
	_datos = torch.Tensor([159.6, 184.6, 208.2, 230.1, 250.7])*1e-7
	_temp = torch.Tensor([250., 300., 350., 400., 450.])
	_visc = interp1d(_temp, _datos, _t)
	_visc[torch.isnan(_visc)] = 0.	
	return _visc
"""

class qParamdrag():
	def __init__(self):
		self._b1 = torch.Tensor([0.039, 0.028, 0.027, 0.028, 0.005])
		self._b2 = torch.Tensor([3.270, 2.416, 2.907, 2.974, 2.063])
		self._x = torch.Tensor([0.10, 0.25, 0.50, 0.75, 1.00])
		self.m1, self.b1 = interp1D(self._x, self._b1)
		self.m2, self.b2 = interp1D(self._x, self._b2)


	def value(self, x):
		_a1 = self.m1*x + self.b1
		_a2 = self.m2*x + self.b2

		mask = x<self._x[-1]

		_a1[mask] = self._b1[-1]
		_a2[mask] = self._b2[-1]

		mask = _a1<0.
		_a1[mask] = 0.

		mask = _a2<0.
		_a1[mask] = 0.
		return _a1, _a2

"""
def q_paramdrag(_s):
	_b1 = torch.Tensor([0.039, 0.028, 0.027, 0.028, 0.005])
	_b2 = torch.Tensor([3.270, 2.416, 2.907, 2.974, 2.063])
	_x = torch.Tensor([0.10, 0.25, 0.50, 0.75, 1.00])
	_a1 = interp1d(_x, _b1, _s)
	_a2 = interp1d(_x, _b2, _s)
	if _s>_x[-1]:
		_a1 = _b1[-1]
		_a2 = _b2[-1]
	if _a1<0.:
		_a1 = 0.
	if _a2<0.:
		_a2 = 0.
	return _a1, _a2
"""

def q_densidad(_t):
	_datos = torch.Tensor([1.293, 1.205, 1.127])
	_temp = torch.Tensor([0., 20., 40.])
	_d = interp1d(_temp, _datos, _t)
	_d[torch.isnan(_d)] = 1.
	return _d


def q_cp(_t):
	_t += 273.15
	_datos = torch.Tensor([1.006, 1.007, 1.009, 1.014, 1.021])*1e3
	_temp = torch.Tensor([250., 300., 350., 400., 450.])
	_cp = interp1d(_temp, _datos, _t)
	_cp[torch.isnan(_cp)] = 1.
	return _cp


def f1drag(_re, _an, _dfn):
	return 0.891*_an - _an**2 + _re**0.1452


def f2drag(_re, _an, _dfn):
	return _an - _an**2 + _re**0.1452


def f3drag(_re, _an, _dfn):
	return 1.5*_an - _an**2 + _re**0.1452


def f1friction(_re, _s, _vmf_vinit, _dfn):
	#print("val", _w*_y**(_x**-0.3891))
	return _dfn*(_s**(24.5261*_re**-0.3891 - 2)).float()


def f2friction(_re, _s, _vmf_vinit, _dfn):
	return _dfn*(_s**(24*_re**-0.3891 - 2)).float()


def f3friction(_re, _s, _vmf_vinit, _dfn):
	return _dfn*(_s**(_re**-0.3891 - 2)).float()


def f1nu(_re):
	return 2.0232*_re**0.5528


def f2nu(_re):
	return _re**0.5528


def f3nu(_re):
	return 4*_re**0.5528


def FMEvaluator(dataInPath, dataOutPath):
	"""
	Evaluates the fenomenological model.
	Inputs:
		dataInPath: string, path to data in.
		dataOutPath: string, path to data out.
		Inds: list, individuals to be evaluated.
	Outputs:
		mse: tensor (nind, ntrees), mean squared error.
	"""
	pobDrag = [f1drag, f2drag, f3drag]
	pobFriction = [f1friction, f2friction, f3friction]
	pobNu = [f1nu, f2nu, f3nu]

	ind1 = [f1drag, f1friction, f1nu]
	ind2 = [f2drag, f2friction, f2nu]
	ind3 = [f3drag, f3friction, f3nu]

	pop = [ind1, ind2, ind1, ind2]

	nind = len(pop)

	data_in  = pd.read_csv(dataInPath, sep=',', header=None, names=['I', 'Separation', 'Fin', 'Tin', 'Diameter'])
	data_out = pd.read_csv(dataOutPath, sep=',', header=None, names=['TFin', 'Tc', 'Pf', 'Vf', 'Dens', 'CoefArr', 'Reynolds', 'Nusselt'])

	ti = time.time()
	
	N, _ = data_out.values.shape
	error = torch.zeros(nind, 3) # nind, number of trees
	E = torch.zeros((N, nind, 3))
	mdl = Model(data_in, nind=nind)
	mdl.uploadPob(pop)
	mdl.reset()
	res = mdl.evolve(10)
	out = torch.Tensor(data_out[['Vf','Pf','Tc']].values)
	mse = MSE(res, out)


	return mse


def MSE(res, out):
	error0 = torch.mean((res[0] - out[:, 0])**2)
	error1 = torch.mean((res[1] - out[:, 1])**2)
	error2 = torch.mean((res[2] - out[:, 2])**2)
	#mse = torch.t(torch.cat((error0, error1, error2)).reshape((3, nind)))
	return error0, error1, error2


if __name__=="__main__":

	pobDrag = [f1drag, f2drag, f3drag]
	pobFriction = [f1friction, f2friction, f3friction]
	pobNu = [f1nu, f2nu, f3nu]

	ind1 = [f1drag, f1friction, f1nu]
	ind2 = [f2drag, f2friction, f2nu]
	ind3 = [f3drag, f3friction, f3nu]

	pop = [ind1, ind2, ind1, ind2]

	nind = len(pop)

	dataInPath = "/home/claudia/Documents/Documents/Uni/primavera_2018/computacion_evolutiva/proyecto/Batteries_GP_MO_Java/TrainingData/doe500trainnew.txt"
	dataOutPath = "/home/claudia/Documents/Documents/Uni/primavera_2018/computacion_evolutiva/proyecto/Batteries_GP_MO_Java/TrainingData/salidas_ansys500trainnew.txt"

	data_in  = pd.read_csv(dataInPath, sep=',', header=None, names=['I', 'Separation', 'Fin', 'Tin', 'Diameter'])
	data_out = pd.read_csv(dataOutPath, sep=',', header=None, names=['TFin', 'Tc', 'Pf', 'Vf', 'Dens', 'CoefArr', 'Reynolds', 'Nusselt'])

	mdl = Model(data_in, nind=nind)
	out = torch.Tensor(data_out[['Vf','Pf','Tc']].values)

	for indv in pop:
		f1 = indv[0] #toolbox.evaluate(indv[0])
		f2 = indv[1] #toolbox.evaluate(indv[1])
		f3 = indv[2] #toolbox.evaluate(indv[2])
		mdl.uploadInd(f1, f2, f3)
		mdl.reset()
		res = mdl.evolve(10)
		# restricciones
		mse = MSE(res, out)
	
	
